import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { calculatorReducer } from "./reducers";

const rootReducer = combineReducers({
    calculatorReducer,
});

const store = configureStore({
    reducer: rootReducer
});

export type RootState = ReturnType<typeof rootReducer>

export default store;
