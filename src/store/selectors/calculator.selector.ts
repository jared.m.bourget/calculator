import { RootState } from '../store';

const selectCalculator = (state: RootState) => state.calculatorReducer.calculator;

export default selectCalculator;
