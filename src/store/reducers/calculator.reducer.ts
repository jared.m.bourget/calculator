import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const INITIAL_STATE = {
	calculator: {
		firstOperand: '',
		secondOperand: '',
		operator: '',
		result: 0,
	}
};

const calculatorSlice = createSlice({
	name: 'calculator',
	initialState: INITIAL_STATE,
	reducers: {
		/**
		 * Appends a digit to either the first or second operand depending on if
		 * there is an operator in state
		 */
		addDigit: (state, action: PayloadAction<string>) => {
			const calc = state.calculator;
			const value = action.payload;
			/**
			 * Messy "if" statements to deal with decimals...
			 */
			if (calc.operator) {
				if (value === "." && !calc.secondOperand.includes(".")) {
					calc.secondOperand += value;
				} else if (value !== ".") {
					calc.secondOperand += value;
				}
			} else {
				if (value === "." && !calc.firstOperand.includes(".")) {
					calc.firstOperand += value;
				} else if (value !== ".") {
					calc.firstOperand += value;
				}
			}
		},

		/**
		 * Removes the last digit from either the first or second operand
		 */
		removeDigit: (state) => {
			const calc = state.calculator;
			if (calc.operator !== "") {
				calc.secondOperand.slice(0, calc.secondOperand.length - 1);
			} else if (calc.operator !== "" && calc.secondOperand === "") {
				calc.operator = "";
			} else {
				calc.firstOperand.slice(0, calc.firstOperand.length - 1);
			}
		},

		/**
		 * resetting state
		 */
		resetCalculator: (state) => {
			state.calculator = INITIAL_STATE.calculator;
		},

		/**
		 * Setting the operator
		 */
		setOperator: (state, action: PayloadAction<string>) => {
			const calc = state.calculator;
			const value = action.payload;

			if (calc.firstOperand !== "" && calc.secondOperand === "") {
				calc.operator = value;
			}
		},

		/**
		 * Calculates result and resets operator/operands
		 * Should probably use a third party library to do math - I don't trust JavaScript with calculations
		 */
		calculate: (state) => {
			const calc = state.calculator;
			let result = 0;
			switch (calc.operator) {
				case "-":
					result = Number(calc.firstOperand) - Number(calc.secondOperand);
					break;
				case "+":
					result = Number(calc.firstOperand) + Number(calc.secondOperand);
					break;
				case "÷":
					result = Number(calc.firstOperand) / Number(calc.secondOperand);
					break;
				case "*":
					result = Number(calc.firstOperand) * Number(calc.secondOperand);
					break;
			}
			state.calculator = {
				...INITIAL_STATE.calculator,
				result,
			}
		}
	}
});

export const {
	addDigit,
	removeDigit,
	setOperator,
	calculate,
	resetCalculator,
} = calculatorSlice.actions;

export default calculatorSlice.reducer;
