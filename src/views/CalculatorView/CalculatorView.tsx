import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import selectCalculator from '../../store/selectors';
import {
  addDigit,
  removeDigit,
  resetCalculator,
  setOperator,
  calculate,
} from '../../store/reducers';

import { CalulatorVariantType } from '../../types';

/**
 * Component Imports
 */
import CalculatorKeypad from '../../components/CalculatorKeypad';
import CalculatorOperators from '../../components/CalculatorOperators';
import CalculatorModifiers from '../../components/CalculatorModifiers';

/**
 * Calculator View
 * @returns 
 */
const CalculatorView = () => {
  const dispatch = useDispatch();
  const calc = useSelector((state: RootState) => selectCalculator(state));
  const {
    firstOperand,
    secondOperand,
    operator,
    result,
  } = calc;

  /**
   * Wanted to have one function to handle all the dispatches
   * @param event
   * @param variant
   */
  const handleClick = (event: React.MouseEvent, variant: CalulatorVariantType) => {
    event.preventDefault();
    const target = event.target as HTMLButtonElement;
    const value = target.value;

    switch(variant) {
      case "addDigit":
        dispatch(addDigit(value));
        break;
      case "removeDigit":
        dispatch(removeDigit());
        break;
      case "setOperator":
        dispatch(setOperator(value));
        break;
      case "resetCalculator":
        dispatch(resetCalculator());
        break;
      case "calculate":
        dispatch(calculate());
        break;
    };
  };

  return (
    <div className="calculator-view-container">
      {/* @TODO - address result/operand spill over */}
      <span className="calculator-output">{
        firstOperand ? `${firstOperand} ${operator} ${secondOperand}` : Number(result.toFixed(6))
      }</span>
      <div className="calculator-view-wrapper">
        <div className="calculator-keypad-container">
          <CalculatorModifiers handleClickEvent={handleClick} />
          <CalculatorKeypad handleClickEvent={handleClick} />
        </div>
        <div className="calculator-operator-container">
          <CalculatorOperators handleClickEvent={handleClick} />
        </div>
      </div>
    </div>
  );
};

export default CalculatorView;
