import React from 'react';
import { CalulatorVariantType } from '../../types';
import CalculatorButton from '../CalculatorButton';

type CalculatorOperatorsPropTypes = {
  handleClickEvent: (event: React.MouseEvent, variant: CalulatorVariantType) => void;
}

const CalculatorOperators = (props: CalculatorOperatorsPropTypes) => {
  const { handleClickEvent } = props;

  return (
    <div className="calculator-operator-wrapper">
      <CalculatorButton
        value="÷"
        handleClickEvent={handleClickEvent}
        variant="setOperator"
        classes='btn-operator'
      >
        &#247;
      </CalculatorButton>
      <CalculatorButton
        value="*"
        handleClickEvent={handleClickEvent}
        variant="setOperator"
        classes='btn-operator'
      >
        &#10005;
      </CalculatorButton>
      <CalculatorButton
        value="-"
        handleClickEvent={handleClickEvent}
        variant="setOperator"
        classes='btn-operator'
      >
        &#8722;
      </CalculatorButton>
      <CalculatorButton
        value="+"
        handleClickEvent={handleClickEvent}
        variant="setOperator"
        classes='btn-operator'
      >
        &#43;
      </CalculatorButton>
      <CalculatorButton
        value="="
        handleClickEvent={handleClickEvent}
        variant="calculate"
        classes='btn-operator'
      >
        &#61;
      </CalculatorButton>
    </div>
  );
};

export default CalculatorOperators;
