import React from 'react';
import { CalulatorVariantType } from '../../types';

import CalculatorButton from '../CalculatorButton';

type CalculatorKeypadPropTypes = {
  handleClickEvent: (event: React.MouseEvent, variant: CalulatorVariantType) => void;
}

const CalculatorKeypad = (props: CalculatorKeypadPropTypes) => {
  const { handleClickEvent } = props;

  return (
    <div className="calculator-keypad-wrapper">
      {[...Array(10)].map((el, index) => (
        <CalculatorButton
          value={`${index}`}
          key={index}
          classes="btn-digit"
          handleClickEvent={handleClickEvent}
          variant="addDigit">
          {index}
        </CalculatorButton>
      ))}
      <CalculatorButton
        value="."
        key="decimal"
        classes="btn-digit"
        handleClickEvent={handleClickEvent}
        variant="addDigit"
      >
        &#183;
      </CalculatorButton>
    </div>
  );
};

export default CalculatorKeypad;
