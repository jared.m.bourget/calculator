import React from 'react';
import { CalulatorVariantType } from '../../types';
import CalculatorButton from '../CalculatorButton';

type CalculatorModifiersPropTypes = {
  handleClickEvent: (event: React.MouseEvent, variant: CalulatorVariantType) => void;
}


/**
 * Resets calculator - all these buttons do the same thing for now
 * @param props 
 * @returns 
 */
const CalculatorModifiers = (props: CalculatorModifiersPropTypes) => {
  const { handleClickEvent } = props;
  return (
    <div className="calculator-modifier-wrapper">
      <CalculatorButton
        value="AC"
        classes="btn-modifier"
        handleClickEvent={handleClickEvent}
        variant="resetCalculator"
      >
        AC
      </CalculatorButton>
      <CalculatorButton
        value="changeSign"
        classes="btn-modifier"
        handleClickEvent={handleClickEvent}
        variant="resetCalculator"
      >
        +/-
      </CalculatorButton>
      <CalculatorButton
        value="percent"
        classes="btn-modifier"
        handleClickEvent={handleClickEvent}
        variant="resetCalculator"
      >
        &#37;
      </CalculatorButton>
    </div>
  );
};

export default CalculatorModifiers;
