import React from 'react';
import { CalulatorVariantType } from '../../types';

type CalculatorButtonPropTypes = {
  children?: React.ReactNode;
  classes: string;
  handleClickEvent: (event: React.MouseEvent, variant: CalulatorVariantType) => void;
  variant: CalulatorVariantType;
  value: string;
}

/**
 * Genericized button component
 * @param props 
 * @returns 
 */
const CalculatorButton = (props: CalculatorButtonPropTypes) => {
  const { classes, handleClickEvent, value, variant, children } = props;

  return (
    <button
      value={value}
      className={`btn-calc ${classes}`}
      onClick={(event) => handleClickEvent(event, variant)}
    >
      {children}
    </button>
  );
};

export default CalculatorButton;
